<?php
$dbhost = "localhost";
$dbuser = "pc4u0fi_username";
$dbpass = "pc4upc4u1";
$dbname = "pc4u0fi_pc4u";

// Create connection
$conn = new mysqli($dbhost, $dbuser, $dbpass, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
if (isset($_POST['submit'])) {
    $voornaam = $_POST['vnaam'];
    $achternaam = $_POST['anaam'];
    $email = $_POST['email'];
    $username = $_POST['username'];
    $wachtwoord = $_POST['wachtwoord'];
    $woonplaats = $_POST['woonplaats'];
    $postcode = $_POST['postcode'];
    $straat = $_POST['straat'];
    $huisnr = $_POST['huisnr'];
    $telnr = $_POST['telnr'];

    if ($voornaam != '' || $email != '') {
        $query = "INSERT INTO
                  `Klanten`(`klant_voornaam`,`klant_achternaam`,`klant_email`,`klant_username`,`klant_wachtwoord`,`klant_woonplaats`,`klant_postcode`,`klant_straat`,`klant_huisnr`,`klant_telefoonnummer`)
                  VALUES ('$voornaam','$achternaam','$email','$username','$wachtwoord','$woonplaats','$postcode','$straat','$huisnr','$telnr')";
        $result = mysqli_query($conn, $query);
    } else {
        echo 'Inserting data failed..';
    }
}
?>

<script>
    function myFunction() {
        alert("U heeft zich succesvol geregistreerd.");
    }
</script>
<link href="contact.css" rel="stylesheet">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<style type="text/css">
    input, td, tr {
        padding-right: 20px;
    }
</style>

<form class="form-horizontal" role="form" method="post">
    <h1 style="margin-left: 10px;">Registreren</h1><br>
    <div class="form-group">
        <label class="control-label col-sm-2" style="text-align: left; width: 120px; margin-left: 20px; margin-top: 10px;" for="vnaam">Voornaam:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="vnaam" name="vnaam" style="width: 250px; margin-top: 10px;" required placeholder="">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="anaam" style="text-align: left; width: 120px; margin-left: 20px; margin-top: 10px;">Achternaam:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="anaam" name="anaam" style="width: 250px; margin-top: 10px;" required placeholder="">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" style="text-align: left; width: 120px; margin-left: 20px; margin-top: 10px;" for="email">Email:</label>
        <div class="col-sm-10">
            <input type="email" class="form-control" id="email" name="email" style="width: 250px; margin-top: 10px;" required placeholder="">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" style="text-align: left; width: 120px; margin-left: 20px; margin-top: 10px;" for="username">Username:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="username" name="username" style="width: 250px; margin-top: 10px;" required placeholder="">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" style="text-align: left; width: 120px; margin-left: 20px; margin-top: 10px;" for="wachtwoord">Wachtwoord:</label>
        <div class="col-sm-10">
            <input type="password" class="form-control" id="wachtwoord" name="wachtwoord" style="width: 250px; margin-top: 10px;" required placeholder="">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" style="text-align: left; width: 120px; margin-left: 20px; margin-top: 10px;" for="woonplaats">Woonplaats:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="woonplaats" name="woonplaats" style="width: 250px; margin-top: 10px;" required placeholder="">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="postcode" style="text-align: left; width: 120px; margin-left: 20px; margin-top: 10px;">Postcode:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="postcode" name="postcode" style="width: 250px; margin-top: 10px;" required placeholder="">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" style="text-align: left; width: 120px; margin-left: 20px; margin-top: 10px;" for="straat">Straat:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="straat" name="straat" style="width: 250px; margin-top: 10px;" required placeholder="">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" style="text-align: left; width: 120px; margin-left: 20px; margin-top: 10px;" for="huisnr">Huisnummer:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="huisnr" name="huisnr" style="width: 250px; margin-top: 10px;" required placeholder="">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" style="text-align: left; width: 120px; margin-left: 20px; margin-top: 10px;" for="telnr">Telefoonnummer:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="telnr" name="telnr" style="width: 250px; margin-top: 10px;" required placeholder="">
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" style="margin-left: 120px; margin-bottom: 20px;" class="btn btn-default" name="submit">Registreren</button>
        </div>
    </div>
</form>