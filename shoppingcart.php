<?php
    require 'Includes/dbconnectie.php';
    require 'functions.php';
    session_start();
    $_totaal = 0;
    $boolIsIngelogt = false;

    if ($_SESSION['ingelogd'] == true)
    {
        $boolIsIngelogt = true;
    }
?>
<script language="javascript">
    function notLoggedIn(eenParam)
    {
        if(<?= $boolIsIngelogt == false ?>)
        {
            window.alert("U moet eerst inloggen.");
            eenParam.stopPropagation();
            return false;
        }
        else
        {
            return true;
        }
    }
</script>
<!--<head>-->
<!--    <link rel="stylesheet" type="text/css" href="index.css">-->
<!--    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">-->
<!--    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>-->
<!--    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>-->
<!--</head>-->
<div class="check" style="margin-left: 90px;">
    <table class="table" class="table table-hover table-bordered" style="width: 800px;">
        <h1>Winkelwagen</h1><br>
        <tr>
            <th style="margin-left: 20px;">Productnaam</th>
            <th style="margin-left: 20px;">Prijs</th>
            <th style="margin-left: 20px;">Aantal</th>
            <th style="margin-left: 20px;">Subtotaal</th>
            <th style="margin-left: 20px;">Verwijderen</th>
        </tr>

        <?php

        ?>

        <?php
        if (isset($_SESSION['winkelwagen']))
        {

                foreach( json_decode($_SESSION['winkelwagen'], true) as $value=>$aantal)
                {
                    $retrieve = "SELECT * FROM Product WHERE ID = $value";
                    $winkelwagen_producten = mysqli_query($conn, $retrieve);
                    echo '<tr>';
                        while($row = mysqli_fetch_assoc($winkelwagen_producten))
                        {
                            echo '<form action="verwijder.php" method="post">';
                            echo '<td><input type="hidden" name="ID" value="'. $row['ID'] .'"/>' . $row['product_naam'] . '</td>';
                            echo '<td>€' . $row['product_prijs'] . ',-</td>';
                            echo '<td>'. $aantal .'</td>';
                            echo '<td>€' . $row['product_prijs'] * $aantal . ',-</td>';
                            echo '<td><input type="submit" value="Verwijder" onclick="return alertBox()" class="btn btn-lg btn-default" style="height: 35px; font-size: 15px; vertical-align: middle;" name="verwijder"/></td>';
                            echo '</form>';
                            $totaal += $row['product_prijs'] * $aantal;
                        }
                    echo '</tr>';
                }

                echo '<tr>';
                    echo '<td></td>';
                    echo '<td></td>';
                    echo '<td><b>TOTAAL:</b></td>';
                    echo '<td>€'. $totaal .',-</td>';
                    echo '<td></td>';
                echo '</tr>';

                echo '<tr>';
                    echo '<form action="leegmaken.php" method="post"/>';
                    echo '<td><input type="submit" value="Leeg maken" onclick="return alertBox()" class="btn btn-lg btn-default" style="height:35px; font-size:15px; vertical-align:middle;" name="Leegmaken"/></td>';
                    echo '</form>';
                    echo '<td></td>';
                    echo '<td></td>';
                    echo '<td></td>';
            if ($_SESSION['ingelogd'] == true)
            {
                echo '<td><input type="submit" value="Afrekenen" onclick="return notLoggedIn(this)" class="btn btn-lg btn-default" style="height:35px; font-size:15px; vertical-align:middle;" name="Afrekenen" 
                          data-toggle="modal" data-target="#myModal"/></td>';
            }
            else
            {
                echo '<td><input type="submit" value="Afrekenen" onclick="return notLoggedIn(this)" class="btn btn-lg btn-default" style="height:35px; font-size:15px; vertical-align:middle;" name="Afrekenen" 
                          /></td>';
            }
                echo '</tr>';

                echo '</table>';
                echo '</div>';



        }
        else
        {
            echo '<div id="cart-list"><div class="alert alert-warning">Uw winkelwagen is leeg</div></div>';
        }
        ?>
        </table>
    </div>
</div>


    <div class="clearfix"> </div>
</div>

<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Bestelling afronden</h4>
            </div>

            <div class="modal-body" style="height: 300px;">
                <?php

                $retrieve = "SELECT * FROM Klanten WHERE ID = '".$_SESSION['ID']."'";
                $result = mysqli_query($conn, $retrieve);
                $data = mysqli_fetch_assoc($result);

                $totaalbedrag = 0;

                foreach( json_decode($_SESSION['winkelwagen'], true) as $value=>$totaalaantal)
                {
                    $retrieve = "SELECT * FROM Product WHERE ID = '".$value ."'";
                    $winkelwagen_producten = mysqli_query($conn, $retrieve);

                    $row = mysqli_fetch_assoc($winkelwagen_producten);
                    $totaalbedrag += $row['product_prijs'] * $totaalaantal;
                }
                ?>
                <table class="table" class="table table-hover table-bordered" style="width: 800px;">
                    <h1>Uw bestelling</h1><br>
                    <tr>
                        <th style="margin-left: 20px;">Productnaam</th>
                        <th style="margin-left: 20px;">Aantal</th>
                        <th style="margin-left: 20px;">Prijs</th>
                    </tr>

                    <?php

                    ?>

                    <?php
                    if (isset($_SESSION['winkelwagen']))
                    {

                        foreach( json_decode($_SESSION['winkelwagen'], true) as $value=>$aantal)
                        {
                            $retrieve = "SELECT * FROM Product WHERE ID = $value";
                            $winkelwagen_producten = mysqli_query($conn, $retrieve);
                            echo '<tr>';
                            while($row = mysqli_fetch_assoc($winkelwagen_producten))
                            {
                                echo '<form action="verwijder.php" method="post">';
                                echo '<td><input type="hidden" name="ID" value="'. $row['ID'] .'"/>' . $row['product_naam'] . '</td>';
                                echo '<td>'. $aantal .'</td>';
                                echo '<td>€' . $row['product_prijs'] . ',-</td>';
                                echo '</form>';
                                $totaal += $row['product_prijs'] * $aantal;
                            }
                            echo '</tr>';
                        }

                        echo '<tr>';
                        echo '<td></td>';
                        echo '<td><b>TOTAAL:</b></td>';
                        echo '<td>€'. $totaalbedrag .',-</td>';
                        echo '</tr>';

                        echo '<tr>';
                        echo '</form>';
                        echo '<td></td>';
                        echo '<td></td>';
                        echo '<td></td>';
                        echo '</tr>';

                        echo '</table>';
                        echo '</div>';
                    }
                    ?>
                </table>
                <form action="afrekenen.php" method="post">
                <h1 style="margin-left: 20px;">Uw gegevens</h1>
                <h3 style="margin-left: 20px;">Als uw gegevens kloppen kunt u de bestelling afronden.</h3><br>
                <h3 style="margin-left: 20px;">Naam </h3><h4 style="margin-left: 20px;"><?php echo $data['klant_voornaam'] . ' ' . $data['klant_achternaam']; ?></h4><br>
                    <h3 style="margin-left: 20px; margin-top: 0px;">Email </h3><h4 style="margin-left: 20px;"><?php echo $data['klant_email']; ?></h4><br>
                    <h3 style="margin-left: 20px; margin-top: 0px;">Adresgegevens </h3><h4 style="margin-left: 20px;"><?php echo $data['klant_straat'] . ' ' . $data['klant_huisnr'] . ' <br> ' . $data['klant_postcode'] . ' ' . $data['klant_woonplaats']; ?></h4>
                <input type="submit" value="Betalen" class="btn btn-lg btn-default" style="height:35px; font-size:15px; vertical-align:middle; margin-top: -50px; margin-right: 20px;float: right;" name="betalingafronden">
                </form>
                <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            </div>
    </div>
</div>