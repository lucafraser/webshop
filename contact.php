<?php
$dbhost = "localhost";
$dbuser = "pc4u0fi_username";
$dbpass = "pc4upc4u1";
$dbname = "pc4u0fi_pc4u";

// Create connection
$conn = new mysqli($dbhost, $dbuser, $dbpass, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
if (isset($_POST['submit'])) {
    $voornaam = $_POST['vnaam'];
    $achternaam = $_POST['anaam'];
    $email = $_POST['email'];
    $subject = $_POST['subject'];
    $message = $_POST['message'];

    if ($voornaam != '' || $email != '') {
        $query = "INSERT INTO `Contact`(`contact_voornaam`, `contact_achternaam`, `contact_email`, `contact_subject`, `contact_message`)
                  VALUES ('$voornaam','$achternaam','$email','$subject','$message')";
        $result = mysqli_query($conn, $query);
        header("location: ?p=c");

    } else {
        echo 'Inserting data failed..';
    }
}
?>

<script>
    function myFunction() {
        alert("U bericht is verzonden. Er word zo spoedig mogelijk contact met u opgenomen.");
    }
</script>

<!--<link href="contact.css" rel="stylesheet">-->
<!--<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">-->
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>-->
<!--<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>-->
<link rel="stylesheet" type="text/css" href="index.css">
<style>
input, td, tr {
        padding-right: 20px;
    }
</style>

<form class="form-horizontal" role="form" method="post">
    <h1 style="margin-left: 10px;">Contact</h1><br>
    <p style="margin-left: 10px;">Voor vragen en of opmerkingen kunt u contact met ons opnemen door middel van het contactformulier hieronder.</p><br>
    <div class="form-group">
        <label class="control-label col-sm-2" style="text-align: left; width: 120px; margin-left: 20px; margin-top: 10px;" for="vnaam">Voornaam:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="vnaam" name="vnaam" style="width: 250px; margin-top: 10px;" required placeholder="">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="anaam" style="text-align: left; width: 120px; margin-left: 20px; margin-top: 10px;">Achternaam:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="anaam" name="anaam" style="width: 250px; margin-top: 10px;" required placeholder="">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" style="text-align: left; width: 120px; margin-left: 20px; margin-top: 10px;" for="email">Email:</label>
        <div class="col-sm-10">
            <input type="email" class="form-control" id="email" name="email" style="width: 250px; margin-top: 10px;" required placeholder="">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" style="text-align: left; width: 120px; margin-left: 20px; margin-top: 10px;" for="subject">Onderwerp:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="subject" name="subject" style="width: 250px; margin-top: 10px;" required placeholder="">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" style="text-align: left; width: 120px; margin-left: 20px; margin-top: 10px;" for="message">Bericht:</label>
        <div class="col-sm-10">
            <textarea type="text" class="form-control" id="message" name="message" style="width: 600px; height: 100px; margin-top: 10px;" required placeholder=""></textarea>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" style="margin-left: 470px; margin-bottom: 20px;" class="btn btn-default" onclick="myFunction()" name="submit">Verzenden</button>
        </div>
    </div>
</form>