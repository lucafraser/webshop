<?php
function redirect($page) {
	header('Location: ' . $page);
}

function logOut() {
	session_destroy();
	redirect('nieuws.php');
}

function stuurMail($mail_naar, $onderwerp, $inhoud) {
	$header = "From:pc4u@pc4u.nl\r\n";
	$header .= "MIME-Version: 1.0\r\n";
	$header .= "Content-type: text/html\r\n";
	
	mail($mail_naar, $onderwerp, $inhoud, $header);
}


?>