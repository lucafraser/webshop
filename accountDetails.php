<?php
session_start();
ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
$dbhost = "localhost";
$dbuser = "pc4u0fi_username";
$dbpass = "pc4upc4u1";
$dbname = "pc4u0fi_pc4u";

// Create connection
$conn = new mysqli($dbhost, $dbuser, $dbpass, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$showalertsuccess = false;
$showalertdanger = false;

$retrieve = "SELECT * FROM Klanten WHERE ID = '".$_SESSION['ID']."'";
$result = mysqli_query($conn, $retrieve);
$data = mysqli_fetch_assoc($result);

if(isset($_POST['submit']))
{
    $voornaam = $_POST['voornaam'];
    $achternaam = $_POST['achternaam'];
    $email = $_POST['email'];
    $username = $_POST['username'];
    $password = $_POST['wachtwoord'];
    $woonplaats = $_POST['woonplaats'];
    $postcode = $_POST['postcode'];
    $straat = $_POST['straat'];
    $huisnr = $_POST['huisnr'];
    $telnr = $_POST['telnr'];

    $query = "UPDATE Klanten SET klant_voornaam='$voornaam', klant_achternaam='$achternaam', klant_email='$email', klant_username='$username', klant_wachtwoord='$password', klant_woonplaats='$woonplaats', klant_postcode='$postcode', klant_straat='$straat', klant_huisnr='$huisnr', klant_telefoonnummer='$telnr' WHERE ID = '".$_SESSION['ID']."'";

    if ($conn->query($query) === TRUE)
    {
        $showalertsuccess = true;
    }
    else
    {
        $showalertdanger = true;
    }
    //var_dump($conn->mysqli_error());
}
?>
<h1 style="margin-left: 10px;">Uw gegevens</h1><br>
<form class="form-horizontal" role="form" method="post">
    <div class="alert alert-success fade in" <?php if($showalertsuccess === false) { ?> style="display:none; width: 410px; margin-left: 20px;" <?php } ?>>
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Gelukt!</strong> Uw account is gewijzigd.
    </div>
    <div class="alert alert-danger fade in" <?php if($showalertdanger === false) { ?> style="display:none; width: 410px; margin-left: 20px;" <?php } ?>>
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Let op!</strong> Er is een fout opgetreden.
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" style="text-align: left; width: 120px; margin-left: 20px; margin-top: 10px;" for="vnaam">Voornaam:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="voornaam" value="<?=$data['klant_voornaam'];?>" name="voornaam" style="width: 250px; margin-top: 10px; margin-left: 40px;" required placeholder="">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="achternaam" style="text-align: left; width: 120px; margin-left: 20px; margin-top: 10px;">Achternaam:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="achternaam" value="<?=$data['klant_achternaam'];?>" name="achternaam" style="width: 250px; margin-top: 10px; margin-left: 40px;" required placeholder="">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" style="text-align: left; width: 120px; margin-left: 20px; margin-top: 10px;" for="email">Email:</label>
        <div class="col-sm-10">
            <input type="email" class="form-control" id="email" value="<?=$data['klant_email'];?>" name="email" style="width: 250px; margin-top: 10px; margin-left: 40px;" required placeholder="">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" style="text-align: left; width: 120px; margin-left: 20px; margin-top: 10px;" for="username">Username:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="username" value="<?=$data['klant_username'];?>" name="username" style="width: 250px; margin-top: 10px; margin-left: 40px;" required placeholder="">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" style="text-align: left; width: 120px; margin-left: 20px; margin-top: 10px;" for="wachtwoord">Wachtwoord:</label>
        <div class="col-sm-10">
            <input type="password" class="form-control" id="wachtwoord" value="<?=$data['klant_wachtwoord'];?>" name="wachtwoord" style="width: 250px; margin-top: 10px; margin-left: 40px;" required placeholder="">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" style="text-align: left; width: 120px; margin-left: 20px; margin-top: 10px;" for="woonplaats">Woonplaats:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="woonplaats" value="<?=$data['klant_woonplaats'];?>" name="woonplaats" style="width: 250px; margin-top: 10px; margin-left: 40px;" required placeholder="">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="postcode" style="text-align: left; width: 120px; margin-left: 20px; margin-top: 10px;">Postcode:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="postcode" value="<?=$data['klant_postcode'];?>" name="postcode" style="width: 250px; margin-top: 10px; margin-left: 40px;" required placeholder="">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" style="text-align: left; width: 120px; margin-left: 20px; margin-top: 10px;" for="straat">Straat:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="straat" value="<?=$data['klant_straat'];?>" name="straat" style="width: 250px; margin-top: 10px; margin-left: 40px;" required placeholder="">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" style="text-align: left; width: 120px; margin-left: 20px; margin-top: 10px;" for="huisnr">Huisnummer:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="huisnr" value="<?=$data['klant_huisnr'];?>" name="huisnr" style="width: 250px; margin-top: 10px; margin-left: 40px;" required placeholder="">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" style="text-align: left; width: 120px; margin-left: 20px; margin-top: 10px;" for="telnr">Telefoonnummer:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="telnr" value="<?=$data['klant_telefoonnummer'];?>" name="telnr" style="width: 250px; margin-top: 10px; margin-left: 40px;" required placeholder="">
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" style="margin-left: 170px; margin-bottom: 20px;" class="btn btn-default" name="submit">Wijzigen</button>
        </div>
    </div>
</form>