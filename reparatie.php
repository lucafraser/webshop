<?php
$dbhost = "localhost";
$dbuser = "pc4u0fi_username";
$dbpass = "pc4upc4u1";
$dbname = "pc4u0fi_pc4u";

// Create connection
$conn = new mysqli($dbhost, $dbuser, $dbpass, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
if (isset($_POST['submit'])) {
    $voornaam = $_POST['vnaam'];
    $achternaam = $_POST['anaam'];
    $factuurnr = $_POST['factuurnr'];
    $probleem = $_POST['probleem'];

    if ($voornaam != '' || $achternaam != '') {
        $query = "INSERT INTO `Reparatieverzoeken`(`klant_vnaam`, `klant_anaam`, `reparatie_factuurnr`, `reparatie_probleem`)
                  VALUES ('$voornaam','$achternaam','$factuurnr','$probleem')";
        $result = mysqli_query($conn, $query);
    } else {
        echo 'Inserting data failed..';
    }
}
?>
<script>
    function myFunction() {
        alert("Uw reparatieverzoek is ingediend. Er word zo spoedig mogelijk contact met u opgenomen.");
    }
</script>
<link rel="stylesheet" type="text/css" href="reparatie.css"/>
<link rel="stylesheet" type="text/css" href="contact.css"/>
<style type="text/css">
    tr, td {
        padding-right: 20px;
    }
</style>
<body>
<div id="content">

    <div id="titel">Toestel Reparaties</div>

    <div id="titel2">Alles over het oplossen van problemen met je toestel.</div>

    <div id="deel_titel_1">Reparatieservice
        <div id="deel_text_1"><br>Op een door PC4U geleverd toestel na 1 januari 2013 zijn de reparatievoorwaarden van
            toepassing. Dit wil zeggen dat je bij PC4U je PC4U product kunt laten repareren. PC4U zorgt er dan voor dat
            je product eerst wordt onderzocht en de aard van het defect wordt vastgesteld. Na het onderzoek ontvang je
            een prijsopgave voor de onderzoekskosten en de reparatie. Pas na uw akkoord op deze prijsopgave wordt de
            reparatie aan het product uitgevoerd en ontvang je het toestel gerepareerd retour.
        </div>
    </div>

    <div id="deel_titel_2">Garantie en Defecten
        <div id="deel_text_2"><br>Wij staan achter onze producten, maar nog belangrijker, achter onze klanten. Voldoet
            een product of levering niet aan uw verwachting of is er een defect opgetreden; u kunt het altijd
            retourneren. Wij garanderen dat de te leveren artikelen voldoen aan de gebruikelijke eisen en normen die
            daaraan gesteld kunnen worden en dat deze vrij zijn van welke gebreken ook.
        </div>
    </div>

    <div id="deel_titel_3">Aanspraak maken op garantie
        <div id="deel_text_3"><br><i>• Defect binnen 8 dagen (Dead On Arrival). <br>• Defect binnen garantie; na 8
                dagen, maar binnen de garantietermijn van het product. <br>• Defect buiten garantie. <br>• Verkeerd
                geleverd, dus een foute levering van PC4U. <br>• Retourneren op basis van de Wet Kopen op Afstand. <br><br>Met
                vragen over deze procedure kunt u mailen naar service@pc4u.nl</i></div>
    </div>

    <div id="tabel_vak">
        <div id="titel_vak">Reparatie verzoek indienen</div>
        <form role="form" method="post" action="?p=r" class="contactForm">
            <table>
                <tr>
                    <td><label for="voornaam">Voornaam:</label></td>
                    <td><input type="text" name="vnaam"></td>
                </tr>
                <tr>
                    <td><label for="achternaam">Achternaam:</label></td>
                    <td><input type="text" name="anaam"></td>
                </tr>
                <tr>
                    <td><label for="factuurnr">Factuurnummer:</label></td>
                    <td><input type="int" name="factuurnr"></td>
                </tr>
                <tr>
                    <td><label for="probleem">Probleem</label></td>
                    <td><textarea rows="3" cols="20" id="probleem" class="" placeholder="" name="probleem""></textarea><br><br></td>
                </tr>
                <tr>
                    <td><button type="submit" class="button" name="submit" onclick="myFunction()">Verzenden</button></td>
                </tr>
            </table>
        </form>
    </div>
</div>
</body>
</html>