<?php
$dbhost = "localhost";
$dbuser = "pc4u0fi_username";
$dbpass = "pc4upc4u1";
$dbname = "pc4u0fi_pc4u";

// Create connection
$conn = new mysqli($dbhost, $dbuser, $dbpass, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$showalertsuccess = false;
$showalertdanger = false;

$retrieve = "SELECT * FROM Product WHERE product_categorie = 'Laptop'";
$result = mysqli_query($conn, $retrieve);


?>
<link type="text/css" rel="stylesheet" href="home.css">
<style type="text/css">
    .btnClass {
        margin-bottom: 10px;
    }
</style>

<div id="container">
    <div class="panel panel-default" style="width:978px;">
        <div class="panel-body">
            <h1 style="margin-top:10px;">Laptops</h1><hr>

            <?php
            while($row = mysqli_fetch_assoc($result)){

                ?>
                <div class="panel-group" style="margin:0px; float: left; margin-bottom: 20px;" >
                    <div class="panel panel-default" style="float:left; width:945px;">
                        <div class="panel-body prod" style="height: 160px;">
                            <form action="InWinkelwagen.php?ID=<?=$row['ID']?>" method="post">
                                <img src="/product_images/<?php echo $row ['product_afbeelding'] ?>" style="width:150px; height:150px;">
                                <h1 style="font-size:16px; margin-left: 200px; margin-top: -120px;"><?php echo $row['product_naam'] ?></h1>
                                <h3 style="float: right;">Prijs: <?php echo "<td>" . "€" . $row['product_prijs'] . "</td>"; ?></h3><br>
                                <p style="float:left; margin-left: 200px; width: 400px;"><?php echo $row['product_omschrijving'] ?></p><br><br><br>
                                <button name="button" style="margin-bottom:0px; float: right;" onclick="" value="<?= $row['ID'] ?>" class="btn btn-lg btn-warning">Bestellen</button>
                            </form>
                            <button name="details" style="margin-bottom:20px; margin-right: 10px; float: right;" onclick="" value="<?php $row ['ID']?><!---->" data-toggle="modal" data-target="#mijnModel<?=$row['ID'] ?>" class="btn btn-lg btn-default">Details</button>

                            <div class="modal fade" id="mijnModel<?=$row['ID'];?>" role="dialog">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title"><?php echo $row ['product_naam'] ?></h4>
                                        </div>

                                        <div class="modal-body" style="height: 330px;">


                                                <img src="/product_images/<?php echo $row ['product_afbeelding'] ?>" style="width:250px; height:250px; float: left;">


                                                <p style="margin-left: 350px;float: left; margin-top: -220px;"><strong>Productbeschrijving:</strong> <br><?php echo "<td>" . $row['product_omschrijving'] . "</td>"; ?></p><br>
                                                <p style="margin-left: 350px;float: left; margin-top: -120px;"><strong>Specificaties:</strong><br> <?php echo "<td>" . $row['product_specs'] . "</td>"; ?></p><br>
                                                <h3 style="margin-left: 350px; float: right; margin-top: -10px;">Prijs: <?php echo "<td>" . "€" . $row['product_prijs'] . "</td>"; ?></h3><br>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>                    <?php  }   ?>
        </div>
    </div>
</div>