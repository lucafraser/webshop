<?php

function isLoggedIn()
{
    if (isset($_SESSION['ingelogd'])) {
        return true;
    }
    return false;
}

function redirect($page)
{
    header('Location: ' . $page);
}

function voegArtikelToeMetAantal($id, $aantal)
{
    if (!isset($_SESSION['Winkelwagen'][$id]) || $_SESSION['Winkelwagen'][$id] == NULL) {
        $_SESSION['Winkelwagen'][$id] = 0;
    }
    $_SESSION['Winkelwagen'][$id] += $aantal;
}

function voegArtikelToeZonderAantal($id)
{
    if (!isset($_SESSION['winkelwagen'][$id]) || $_SESSION['winkelwagen'][$id] == NULL)
    {
        $_SESSION['winkelwagen'][] = $id;
    }
    //$_SESSION['Winkelwagen'][$id];
}

function verwijderArtikel($id)
{
    unset($_SESSION['Winkelwagen'][$id]);
}
?>

<script language="javascript">
    function alertBox()
    {
        if(confirm("Weet u zeker dat u dit wilt verwijderen?"))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    function confirmDeleteFunction(param)
    {
        if(confirm("Weet je zeker dat je dit wilt verwijderen"))
        {
            location.href = param;
            return true;
        }
        else
        {
            return false;
        }
    }
</script>