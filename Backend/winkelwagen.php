<!DOCTYPE html>

<?php
    require 'Includes/dbconnectie.php'
?>

<html>
  <head>
    <meta charset="utf-8">
    <title></title>
	<link href="css\globaal.css" rel="stylesheet" type="text/css" />
	<link href="css\winkelwagen.css" rel="stylesheet" type="text/css" />
  </head>
  <body>
	<div id="main">
		<?php require 'includes/advertenties.php'; ?> 
		<div id="container">
			<?php require 'includes/header.php'; ?>
			
			<?php require 'includes/menu.php'; ?>
			
			<div id="content">
				<div id="">
					<form action="" method="post">
						<input class="" id="" type="submit" value="Verder winkelen" /> <br />
					</form>
					<br />
				</div>
				<?php 
				$totaal = 0;
				
				if (isset($_SESSION['Winkelwagen'])) {
					if(isLoggedIn()) {
							echo '<div>';
							echo '<table id="producten">';
								echo '<tr>';
									echo '<th class="titel"></th>';
									echo '<th class="titel">Productnaam</th>';
									echo '<th class="titel">Prijs</th>';
									echo '<th class="titel">Aantal</th>';
									echo '<th class="titel">Subtotaal</th>';
									echo '<th class="titel">Verwijderen</th>';
								echo '</tr>';
								foreach($_SESSION['Winkelwagen'] as $productID => $aantal) {
									echo '<tr>';
									$producten_winkelwagen = mysql_query("SELECT * FROM Product WHERE Productnummer = $productID");
									while ($row = mysql_fetch_array($producten_winkelwagen)) {
										echo '<form action="verkopen.php" method="post">';
											echo '<td class="vak"><img src="' . $row['Afbeelding_groot'] . '" alt="Product"/></td>';
											echo '<td class="vak"><input type="hidden" name="productID" value="' . $row['Productnummer'] . '"/>' . $row['Productnaam'] . '</td>';
											echo '<td class="vak">€' . $row['Prijs'] . '</td>';
											echo '<td class="vak"><p class="aantal">' . $aantal . '</td>';
											echo '<td class="vak">€' . $aantal * $row['Prijs'] . '</td>';
											$totaal += $aantal * $row['Prijs'];
											echo '<td class="vak"><input class="button" type="submit" value="Verwijder" name="verwijder"</td>';
										echo '</form>';
									}
								}
							echo '</table>';
							echo '</div>';
							echo '<div>';
								echo '<table id="minitable">';
									echo '<tr>';
										echo '<th>Eindtotaal:</th>';
										echo '<td>€' . $totaal . '</td>';
									echo '</tr>';
								echo '</table>';
									echo '<div id="afrekenen">';
										echo '<form action="afrekenen.php" method="post">';
											echo '<input class="terug" type="submit" name="afrekenen" value="Afrekenen"/>';
										echo '</form>';
									echo '</div>';
									echo '<div id="bereken">';
										echo '<input class="terug" type="button" value="Herbereken bedrag"/>';
									echo '</div>';
							echo '</div>';
						} else {
							echo 'U moet eerst inloggen';
						}
					} else {
						echo 'U heeft nog niks in uw winkelwagen';
					}
				?>
			</div>
			
			<div id="empty">
			
			</div>
		</div>
	</div>
  </body>
</html>
