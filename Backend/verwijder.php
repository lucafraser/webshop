<?php
    session_start();
    require 'functions.php';

    $id = $_POST['ID'];

    $zoek = json_decode($_SESSION['winkelwagen'], true);
    $key = array_search($id, $zoek /*json_decode($_SESSION['winkelwagen'], true)*/);

    unset(/*$_SESSION['winkelwagen']*/ $zoek[$id]);

    $_SESSION['winkelwagen'] = json_encode($zoek);

    redirect('/?p=w');
?>