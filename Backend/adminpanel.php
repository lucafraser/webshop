<?php
session_start();
include 'functions.php';

if(!isset($_SESSION['login']))
{
    header('Location: http://pc4u.hexodo.nl/Backend');
}

$dbhost = "localhost";
$dbuser = "pc4u0fi_username";
$dbpass = "pc4upc4u1";
$dbname = "pc4u0fi_pc4u";

// Create connection
$conn = new mysqli($dbhost, $dbuser, $dbpass, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
?>

<html lang="en"><head>
    <meta charset="UTF-8">
    <title>Admin panel</title>
    <link rel="stylesheet" type="text/css" href="tables.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="index.css">

</head>
<body>
<div id="content">
    <ul>
        <li><a href="adminpanel.php">Admin panel</a></li>
        <li><a href="Medewerkersoverzicht.php">Medewerkersoverzicht</a></li>
        <li><a href="addMedewerker.php">Medewerkers toevoegen</a></li>
        <li><a href="Klantenoverzicht.php">Klantenoverzicht</a></li>
        <li><a href="Productoverzicht.php">Productoverzicht</a></li>
        <li><a href="addProduct.php">Product toevoegen</a></li>
        <li><a href="reparatieOverzicht.php">Reparatieoverzicht</a></li>
        <li><a href="contactoverzicht.php">Contactoverzicht</a></li>
        <li class="floatLi"><a href="logout.php">Uitloggen</a></li>
    </ul>
    <h1 class="h1Center">Admin panel</h1>
    <img src="images/logo.jpg" class="center">
    </div>
</html>