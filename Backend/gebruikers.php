<?php
require"../Includes/dbconnectie.php";
require"../login.php";
$sql = "SELECT * FROM User WHERE user_admin = 1";

$result = mysqli_query($database, $sql);
?>

<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Gebruikers</title>
    <link href="gebruikers.css" rel="stylesheet" type="text/css">
</head>

<body>
<div id="main">
    <h1>Gebruikers Tabel</h1>
    <table border='1'>
        <tr>
            <th>Voornaam</th>
            <th>Achternaam</th>
            <th>Email</th>
            <th>Woonplaats</th>
            <th>Postcode</th>
            <th>Straat</th>
            <th>Huisnummer</th>
            <th>Telefoonnummer</th>
        </tr>

        <?php

        while($row = mysqli_fetch_array($result)) {
            echo "<tr>";
            echo "<td>" . $row['user_voornaam'] . "</td>";
            echo "<td>" . $row['user_achternaam'] . "</td>";
            echo "<td>" . $row['user_email'] . "</td>";
            echo "<td>" . $row['user_woonplaats'] . "</td>";
            echo "<td>" . $row['user_postcode'] . "</td>";
            echo "<td>" . $row['user_straat'] . "</td>";
            echo "<td>" . $row['user_huisnr'] . "</td>";
            echo "<td>" . $row['user_telefoonnummer'] . "</td>";
            echo "</tr>";
        }
        ?>

    </table>

</div>
</body>
</html>