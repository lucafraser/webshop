<?php
session_start();

if(!isset($_POST['username']))
    $_POST['username'] = '';
if(!isset($_POST['password']))
    $_POST['password'] = '';

if (isset($_SESSION['ingelogd'])) header("location: adminpanel.php");


$dbhost = "localhost";
$dbuser = "pc4u0fi_username";
$dbpass = "pc4upc4u1";
$dbname = "pc4u0fi_pc4u";
$conn = new mysqli($dbhost, $dbuser, $dbpass, $dbname);

if ($conn->connect_error) die("Connection failed");

if (isset($_POST['submit'])) {

    $uname = $_POST['username'];
    $wwoord = $_POST['password'];


    $query = 'SELECT * FROM Medewerkers WHERE medewerker_username = "' . $uname . '" && medewerker_password = "' . $wwoord . '"';

    $result = $conn->query($query);

    if ($result->num_rows == 1) {
        $result = $result->fetch_row();
        $_SESSION['login'] = true;
        $_SESSION['admin'] = $result['medewerker_admin'];
        header("location: adminpanel.php");
    } else {
        $msg = "Inloggegevens incorrect.";
    }
    //$conn->close();
}

?>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Admin login</title>
    <link rel="stylesheet" type="text/css" href="tables.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
<div id="content">
    <ul>
        <li><a href="index.php">Admin panel</a></li>
    </ul>
    <h1>Admin login</h1>
    <?php
    echo $msg;
    ?>
    <form method="post">
        <table>
            <tr>
                <td><label for="username">Username</label></td>
                <td><input type="text" value="<?= $_POST['username']; ?>" name="username" class="" id="username"><br><br></td>
            </tr>
            <tr>
                <td><label for="wachtwoord">Wachtwoord</label></td>
                <td><input type="password" value="<?= $_POST['password']; ?>" name="password" class="" id="wachtwoord"><br><br></td>
            </tr>
            <tr>
                <td>
                    <button type="submit" name="submit" class="button">Inloggen</button><br>
                </td>
            </tr>
        </table>
    </form>
</div>
</body>
</html>