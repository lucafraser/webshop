<?php
include 'functions.php';
session_start();
if(!isset($_SESSION['login']))
{
    header('Location: http://pc4u.hexodo.nl/Backend');
}
$dbhost = "localhost";
$dbuser = "pc4u0fi_username";
$dbpass = "pc4upc4u1";
$dbname = "pc4u0fi_pc4u";

$showalertsuccess = false;
$showalertdanger = false;

// Create connection
$conn = new mysqli($dbhost, $dbuser, $dbpass, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
if (isset($_POST['submit'])) {
    $voornaam = $_POST['vnaam'];
    $achternaam = $_POST['anaam'];
    $email = $_POST['email'];
    $admin = $_POST['admin'];
    $username = $_POST['username'];
    $password = $_POST['password'];

    if ($voornaam != '' || $email != '') {
        $query = "INSERT INTO `Medewerkers`(`medewerker_voornaam`,`medewerker_achternaam`,`medewerker_email`,`medewerker_admin`,`medewerker_username`,`medewerker_password`)
                  VALUES ('$voornaam','$achternaam','$email','$admin','$username','$password')";
        $result = mysqli_query($conn, $query);
        $showalertsuccess = true;

    } else {
        echo 'Inserting data failed..';
        $showalertdanger = true;
    }
}

?>
<html lang="en"><head>
    <meta charset="UTF-8">
    <title>Medewerkers toevoegen</title>
    <link rel="stylesheet" type="text/css" href="tables.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="index.css">


    <script type="text/javascript">
        function myFunction() {
            alert("De medewerker is succesvol toegevoegd");
        }
    </script>
</head>
<body>
<div id="content">
<ul>
    <li><a href="adminpanel.php">Admin panel</a></li>
    <li><a href="Medewerkersoverzicht.php">Medewerkersoverzicht</a></li>
    <li><a href="addMedewerker.php">Medewerkers toevoegen</a></li>
    <li><a href="Klantenoverzicht.php">Klantenoverzicht</a></li>
    <li><a href="Productoverzicht.php">Productoverzicht</a></li>
    <li><a href="addProduct.php">Product toevoegen</a></li>
    <li><a href="reparatieOverzicht.php">Reparatieoverzicht</a></li>
    <li><a href="contactoverzicht.php">Contactoverzicht</a></li>
    <li class="floatLi"><a href="logout.php">Uitloggen</a></li>
</ul>
</html>


<div class="container" style="margin: 0; padding: 0;">
    <h1>Medewerker toevoegen</h1>
    <br>
    <div class="alert alert-success fade in" <?php if($showalertsuccess === false) { ?> style="display:none" <?php } ?>>
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Gelukt!</strong> De medewerker is succesvol toegevoegd!
    </div>
    <div class="alert alert-danger fade in" <?php if($showalertdanger === false) { ?> style="display:none" <?php } ?>>
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Let op!</strong> Er is een fout opgetreden. Neem contact op met de beheerder van deze website als dit zich voor blijft doen!
    </div>
    <form class="form-horizontal" role="form" method="post">
        <div class="form-group">
            <label class="control-label col-sm-2" style="text-align: left; width: 120px;" for="vnaam">Voornaam:</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="vnaam" name="vnaam" style="width: 250px;" required placeholder="">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="anaam" style="text-align: left; width: 120px;">Achternaam:</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="anaam" name="anaam" style="width: 250px;" required placeholder="">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" style="text-align: left; width: 120px;" for="email">Email:</label>
            <div class="col-sm-10">
                <input type="email" class="form-control" id="email" name="email" style="width: 250px;" required placeholder="">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" style="text-align: left; width: 120px;" for="admin">Admin:</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="admin" name="admin" style="width: 250px;" required placeholder="1 = true | 0 = false">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" style="text-align: left; width: 120px;" for="username">Username:</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="username" name="username" style="width: 250px;" required placeholder="">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" style="text-align: left; width: 120px;" for="password">Password:</label>
            <div class="col-sm-10">
                <input type="password" class="form-control" id="password" name="password" style="width: 250px;" required placeholder="">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" style="margin-left: 80px;" class="btn btn-default" name="submit">Toevoegen</button>
            </div>
        </div>
    </form>
</div>