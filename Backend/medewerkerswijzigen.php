<?php
include 'functions.php';
session_start();
if(!isset($_SESSION['login']))
{
    header('Location: http://pc4u.hexodo.nl/Backend');
}
$dbhost = "localhost";
$dbuser = "pc4u0fi_username";
$dbpass = "pc4upc4u1";
$dbname = "pc4u0fi_pc4u";

// Create connection
$conn = new mysqli($dbhost, $dbuser, $dbpass, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$showalertsuccess = false;
$showalertdanger = false;

$retrieve = "SELECT * FROM Medewerkers WHERE ID = '".$_GET['wijzigen_id']."'";
$result = mysqli_query($conn, $retrieve);
$data = mysqli_fetch_assoc($result);

$minion = $_GET['wijzigen_id'];

if(isset($_POST['submit']))
{
    $voornaam = $_POST['vnaam'];
    $achternaam = $_POST['anaam'];
    $email = $_POST['email'];
    $admin = $_POST['admin'];
    $username = $_POST['username'];
    $password = $_POST['password'];

    $query = "UPDATE Medewerkers SET medewerker_voornaam='$voornaam', medewerker_achternaam='$achternaam', medewerker_email='$email', medewerker_admin='$admin', medewerker_username='$username', medewerker_password='$password' WHERE ID = '".$_GET['wijzigen_id']."'";

    if ($conn->query($query) === TRUE)
    {
        $showalertsuccess = true;
    }
    else
    {
        $showalertdanger = true;
    }
}
?>
<html lang="en"><head>
    <meta charset="UTF-8">
    <title>Medewerkers toevoegen</title>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="index.css">
    <link rel="stylesheet" type="text/css" href="tables.css">

        <script type="text/javascript">
        function myFunction1() {
            alert("");
        }
    </script>
</head>
<body>
<div id="content">
    <ul>
        <li><a href="adminpanel.php">Admin panel</a></li>
        <li><a href="Medewerkersoverzicht.php">Medewerkersoverzicht</a></li>
        <li><a href="addMedewerker.php">Medewerkers toevoegen</a></li>
        <li><a href="Klantenoverzicht.php">Klantenoverzicht</a></li>
        <li><a href="Productoverzicht.php">Productoverzicht</a></li>
        <li><a href="addProduct.php">Product toevoegen</a></li>
        <li><a href="reparatieOverzicht.php">Reparatieoverzicht</a></li>
        <li><a href="contactoverzicht.php">Contactoverzicht</a></li>
        <li class="floatLi"><a href="logout.php">Uitloggen</a></li>
    </ul>
</html>

<h1>Medewerker wijzigen</h1>
<br>
<form class="form-horizontal" method="post">
    <div class="alert alert-success fade in" <?php if($showalertsuccess === false) { ?> style="display:none" <?php } ?>>
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Gelukt!</strong> De medewerker is succesvol bijgewerkt.
    </div>
    <div class="alert alert-danger fade in" <?php if($showalertdanger === false) { ?> style="display:none" <?php } ?>>
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Let op!</strong>Er is een fout opgetreden.
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" style="text-align: left; width: 120px;">Voornaam:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="vnaam" value="<?=$data['medewerker_voornaam'];?>" style="width: 250px;" required placeholder="">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" style="text-align: left; width: 120px;">Achternaam:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="anaam" value="<?=$data['medewerker_achternaam'];?>" style="width: 250px;" required placeholder="">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" style="text-align: left; width: 120px;">Email:</label>
        <div class="col-sm-10">
            <input type="email" class="form-control" name="email" value="<?=$data['medewerker_email'];?>" style="width: 250px;" required placeholder="">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" style="text-align: left; width: 120px;">Admin:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="admin" value="<?=$data['medewerker_admin'];?>" style="width: 250px;" required placeholder="1 = true | 0 = false">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" style="text-align: left; width: 120px;">Username:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="username" value="<?=$data['medewerker_username'];?>"style="width: 250px;" required placeholder="">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" style="text-align: left; width: 120px;">Password:</label>
        <div class="col-sm-10">
            <input type="password" class="form-control" name="password" value="<?=$data['medewerker_password'];?>" style="width: 250px;" required placeholder="">
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" style="margin-left: -30px;" class="btn btn-default" name="submit">Wijzigen</button>
        </div>
    </div>
</form>