<?php
include '../functions.php';
session_start();
if(!isset($_SESSION['login']))
{
    header('Location: http://pc4u.hexodo.nl/Backend');
}
$dbhost = "localhost";
$dbuser = "pc4u0fi_username";
$dbpass = "pc4upc4u1";
$dbname = "pc4u0fi_pc4u";

// Create connection
$conn = new mysqli($dbhost, $dbuser, $dbpass, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
if ($_GET['delete'] == true) {
    $sql1 = "DELETE FROM Product WHERE ID = '" . $_GET['ID'] . "'";
    $done = $conn->query($sql1);

    if ($done) {
        header("location: Productoverzicht.php");
    }
}
?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Product overzicht</title>
    <link rel="stylesheet" type="text/css" href="index.css">
    <link rel="stylesheet" type="text/css" href="tables.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <link href="../contact.css" rel="stylesheet">
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script language="JavaScript" type="text/javascript">
        $(document).ready(function(){
            $("a.delete").click(function(e){
                if(!confirm('Are you sure?')){
                    e.preventDefault();
                    return false;
                }
                return true;
            });
        });
    </script>
</head>
<body>
<div id="content" style="overflow: scroll;">
<ul>
    <li><a href="adminpanel.php">Admin panel</a></li>
    <li><a href="Medewerkersoverzicht.php">Medewerkersoverzicht</a></li>
    <li><a href="addMedewerker.php">Medewerkers toevoegen</a></li>
    <li><a href="Klantenoverzicht.php">Klantenoverzicht</a></li>
    <li><a href="Productoverzicht.php">Productoverzicht</a></li>
    <li><a href="addProduct.php">Product toevoegen</a></li>
    <li><a href="reparatieOverzicht.php">Reparatieoverzicht</a></li>
    <li><a href="contactoverzicht.php">Contactoverzicht</a></li>
    <li class="floatLi"><a href="logout.php">Uitloggen</a></li>
</ul>
    <h1>Productoverzicht</h1>
<br>
    <div id="overzicht">
<form action="index.php">
    <table class="table" class="table table-hover table-bordered" style="width: 1200px; border-color: transparent;">

    <tr>
            <th>ID</th>
            <th>Product afbeelding</th>
            <th>Product naam</th>
            <th>Product omschrijving</th>
            <th>Product specificaties</th>
            <th>Product categorie</th>
            <th>Product actie</th>
            <th>Product prijs</th>
            <th colspan="2" style="width:50px;">Opties</th>

        </tr>
        <?php
        $query = "SELECT * FROM Product";
        $result = $conn->query($query);

        while ($row = mysqli_fetch_array($result)) {
            echo "<tr class='warning'>";
            echo "<td>" . $row['ID'] . "</td>";
            echo "<td>" . $row['product_afbeelding'] . "</td>";
            echo "<td>" . $row['product_naam'] . "</td>";
            echo "<td>" . $row['product_omschrijving'] . "</td>";
            echo "<td>" . $row['product_specs'] . "</td>";
            echo "<td>" . $row['product_categorie'] . "</td>";
            echo "<td>" . $row['product_actie'] . "</td>";
            echo "<td>" . "€" . $row['product_prijs'] . "</td>";
            ?>
            <td style="width:25px;"><input type="button" style="margin:0px;" onclick='location.href="productwijzigen.php?wijzigen_id=<?= $row['ID'] ?>"' name="Wijzigen" class="btn btn-primary btn-sm" value="Wijzigen"/></td>
            <td style="width:25px;"><input type="button" style="margin:0px;" onclick='confirmDeleteFunction("?delete=true&ID=<?= $row['ID'] ?>") /*,function()*/' name="Verwijderen" class="btn btn-danger btn-sm" value="Verwijderen"/></td>
        <?php
            echo "</tr>";
        }
        ?>
    </table>
</form>
    </div>
</div>
</body>
</html>