<?php
include 'functions.php';
session_start();
if(!isset($_SESSION['login']))
{
    header('Location: http://pc4u.hexodo.nl/Backend');
}
$dbhost = "localhost";
$dbuser = "pc4u0fi_username";
$dbpass = "pc4upc4u1";
$dbname = "pc4u0fi_pc4u";

// Create connection
$conn = new mysqli($dbhost, $dbuser, $dbpass, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
$showalertsuccess = false;
$showalertdanger = false;

if (isset($_POST['submit'])) {

    $naam = $_POST['naam'];
    $omschrijving = $_POST['omschrijving'];
    $specs = $_POST['specs'];
    $categorie = $_POST['categorie'];
    $actie = $_POST['actie'];
    $afbeelding = $_POST['afbeelding'];
    $prijs = $_POST['prijs'];




    $uploadPath = $_SERVER['DOCUMENT_ROOT']."/product_images/";
    $target_file = $uploadPath . basename($_FILES["afbeelding"]["name"]);
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
    $check = getimagesize($_FILES["afbeelding"]["tmp_name"]);
    if($check !== false) {
        move_uploaded_file($_FILES["afbeelding"]["tmp_name"],$target_file);
    }

    if ($naam != '' || $prijs != '') {
        $query = "INSERT INTO `Product`(`product_naam`, `product_omschrijving`,`product_specs`, `product_categorie`,`product_actie`, `product_afbeelding`, `product_prijs`)
                  VALUES ('$naam','$omschrijving','$specs','$categorie','$actie','". basename($_FILES["afbeelding"]["name"])."', '$prijs')";
        $result = mysqli_query($conn, $query);
        //header("location: addProduct.php");
        $showalertsuccess = true;

    } else {
        echo 'Inserting data failed..';
        $showalertdanger = true;
    }
}
?>
<html lang="en"><head>
    <meta charset="UTF-8">
    <title>Product toevoegen</title>
    <link rel="stylesheet" type="text/css" href="tables.css">
    <link rel="stylesheet" type="text/css" href="index.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
<div id="content">
<ul>
    <li><a href="adminpanel.php">Admin panel</a></li>
    <li><a href="Medewerkersoverzicht.php">Medewerkersoverzicht</a></li>
    <li><a href="addMedewerker.php">Medewerkers toevoegen</a></li>
    <li><a href="Klantenoverzicht.php">Klantenoverzicht</a></li>
    <li><a href="Productoverzicht.php">Productoverzicht</a></li>
    <li><a href="addProduct.php">Product toevoegen</a></li>
    <li><a href="reparatieOverzicht.php">Reparatieoverzicht</a></li>
    <li><a href="contactoverzicht.php">Contactoverzicht</a></li>
    <li class="floatLi"><a href="logout.php">Uitloggen</a></li>
</ul>
</html>
<h1>Product toevoegen</h1>
<br>
<form class="form-horizontal" method="post" enctype="multipart/form-data">
    <div class="alert alert-success fade in" <?php if($showalertsuccess === false) { ?> style="display:none" <?php } ?>>
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Gelukt!</strong> Het product is succesvol toegevoegd!
    </div>
    <div class="alert alert-danger fade in" <?php if($showalertdanger === false) { ?> style="display:none" <?php } ?>>
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Let op!</strong>Er is een fout opgetreden.
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" style="text-align: left; width: 120px; padding-top: 0px;">Product naam:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="naam" style="width: 250px;" required placeholder="">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" style="text-align: left; width: 120px; padding-top: 0px;">Product omschrijving:</label>
        <div class="col-sm-10">
            <textarea type="text" class="form-control" name="omschrijving" style="width: 250px; height: 100px;" required placeholder=""></textarea>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" style="text-align: left; width: 120px; padding-top: 0px;">Product specificaties:</label>
        <div class="col-sm-10">
            <textarea type="text" class="form-control" name="specs" style="width: 250px;" required placeholder=""></textarea>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" style="text-align: left; width: 120px; padding-top: 0px;">Product categorie:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="categorie" style="width: 250px;" required placeholder="">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" style="text-align: left; width: 120px; padding-top: 0px;">Product actie:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="actie" style="width: 250px;" required placeholder="">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" style="text-align: left; width: 120px; padding-top: 0px;">Product prijs:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="prijs" style="width: 250px;" required placeholder="">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" style="text-align: left; width: 120px; padding-top: 0px;">Product afbeelding:</label>
        <div class="col-sm-10">
            <input type="file" class="form-control" name="afbeelding" style="width: 250px;" required placeholder="">
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" style="margin-left: -50px;" class="btn btn-default" name="submit">Toevoegen</button>
        </div>
    </div>

</div>
    </form>