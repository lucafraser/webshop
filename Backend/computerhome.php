<?php

?>
<link type="text/css" rel="stylesheet" href="home.css">
<style type="text/css">
    .btnClass {
        margin-bottom: 10px;
    }
</style>

<div id="container">
    <div class="panel panel-default" style="width:978px;">
        <div class="panel-body">
            <h1 style="margin-top:10px;">Aanbiedingen</h1><hr>
            <div class="panel-group" style="margin:0px; float: left;">
                <div class="panel panel-default" style="float:left; width:300px;">
                    <div class="panel-body prod" style="height: 130px;">
                        <div id="prodLinks">
                            <img src="images/V7_V5-touch-black-product-sku-main.png" alt="Foto" style="width:150px; height:100px;">
                        </div>
                        <div id="prodRechts">
                            <h1 style="font-size:16px;">Acer aspire</h1>
                            <h2 style="font-size:14px;">V5-572G9</h2>
                            <button name="toProduct" style="margin-bottom:4px;" onclick="" class="btnClass">Bestellen</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-group" style="margin:0px;float: left; margin-left: 20px; margin-bottom: 20px;">
                <div class="panel panel-default" style="float:left; width:300px;">
                    <div class="panel-body prod" style="height: 130px;">
                        <div id="prodLinks">
                            <img src="images/acer.jpg" alt="Foto" style="width:120px; height:100px;">
                        </div>
                        <div id="prodRechts">
                            <h1 style="font-size:16px;">ACER ASPIRE</h1>
                            <h2 style="font-size:14px;">XC-704 I3940</h2>
                            <button name="toProduct" style="margin-bottom:4px;" onclick="" class="btnClass">Bestellen</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel-group" style="margin:0px;float: left; margin-left: 20px;">
                <div class="panel panel-default" style="float:left; width:300px;">
                    <div class="panel-body prod" style="height: 130px;">
                        <div id="prodLinks">
                            <img src="images/440808.jpg" alt="Foto" style="width:120px; height:100px;">
                        </div>
                        <div id="prodRechts">
                            <h1 style="font-size:16px;">HP</h1>
                            <h2 style="font-size:14px;">15-AF122ND</h2>
                            <button name="toProduct" style="margin-bottom:4px;" onclick="" class="btnClass">Bestellen</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel-group" style="margin:0px;float: left; margin-left: 20px;">
                <div class="panel panel-default" style="float:left; width:300px;">
                    <div class="panel-body prod" style="height: 130px;">
                        <div id="prodLinks">
                            <img src="images/440808.jpg" alt="Foto" style="width:120px; height:100px;">
                        </div>
                        <div id="prodRechts">
                            <h1 style="font-size:16px;">HP</h1>
                            <h2 style="font-size:14px;">15-AF122ND</h2>
                            <button name="toProduct" style="margin-bottom:4px;" onclick="" class="btnClass">Bestellen</button>
                        </div>
                    </div>
                </div>
            </div>



        </div>

    </div>

</div>