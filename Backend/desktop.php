<?php
$dbhost = "localhost";
$dbuser = "pc4u0fi_username";
$dbpass = "pc4upc4u1";
$dbname = "pc4u0fi_pc4u";

// Create connection
$conn = new mysqli($dbhost, $dbuser, $dbpass, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$showalertsuccess = false;
$showalertdanger = false;

$retrieve = "SELECT * FROM Product WHERE product_categorie = 'desktop'";
$result = mysqli_query($conn, $retrieve);


?>
<link type="text/css" rel="stylesheet" href="home.css">
<style type="text/css">
    .btnClass {
        margin-bottom: 10px;
    }
</style>

<div id="container">
    <div class="panel panel-default" style="width:978px;">
        <div class="panel-body">
            <h1 style="margin-top:10px;">Alle Desktops</h1>
            <hr>

            <?php
            while ($row = mysqli_fetch_assoc($result)) {

                ?>
                <div class="panel-group" style="margin:0px; float: left; margin-bottom: 20px;">
                <div class="panel panel-default" style="float:left; width:945px; margin-bottom: 30px;">
                    <div class="panel-body prod" style="height: 150px;">

                        <h1 style="font-size:16px; margin-left: 150px;"><?php echo $row['product_naam'] ?></h1>
                        <p style="margin-left: 150px;"><?php echo $row['product_omschrijving'] ?></p>
                        <button name="toProduct" style="margin-bottom:4px; float: right;" onclick=""
                                value="<?php $row['ID'] ?>" class="btnClass">Bestellen
                        </button>
                        <br>

                    </div>
                </div>

                </div><?php } ?>
        </div>
    </div>
</div>