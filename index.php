<?php
session_start();
$dbhost = "localhost";
$dbuser = "pc4u0fi_username";
$dbpass = "pc4upc4u1";
$dbname = "pc4u0fi_pc4u";

// Create connection
$conn = new mysqli($dbhost, $dbuser, $dbpass, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
//$query = "SELECT * FROM Klanten WHERE ID = '".$_GET['wijzigen_id']."'";
//$result = $conn->query($query);
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>PC4U</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <link href="index.css" rel="stylesheet">
</head>
<script>
    function myFunction() {
        alert("U bent succesvol uitgelogd");
    }
</script>

<body>
<div id="container1">
    <header>
        <div id="headerFotoDiv">
            <img src="images/logo.jpg" height="100px" class="lihover"
                 onclick="window.location='http://www.pc4u.hexodo.nl/'">
        </div>
        <?php
        if ($_SESSION['ingelogd']) { // $_SESSION=['wijzigid'] = $row['ID'];

            ?>

            <div id="headerLogindiv">
            <button class="btn btn-warning" onclick="location.href='?p=ac'"
                    style="width: 130px; height:30px; margin-left: 160px; margin-top: 12px; text-align: center;">Account
            </button>
            <button class="btn btn-warning" onclick="window.location='?p=lo', myFunction()"
                    style="width: 130px; height:30px; margin-left: 160px; margin-top: 12px; text-align: center;">
                Uitloggen
            </button>
            </div>
            <?php
        } else {
            ?>
            <div id="headerLogindiv">
                <button class="btn btn-warning" onclick="window.location='?p=i'"
                        style="width: 130px; height:30px; margin-left: 160px; margin-top: 12px; text-align: center;">
                    Inloggen
                </button>
                <button class="btn btn-warning" onclick="window.location='?p=re'"
                        style="width: 130px; height:30px; margin-left: 160px; margin-top: 15px; text-align: center;">
                    Registreren
                </button>
            </div>
            <?php
        }
        ?>

    </header>
    <!-- Static navbar -->
    <nav class="navbar navbar-default"
         style="width: 1000px; margin-bottom:0px;padding-bottom:0px;padding-top:0px;border:0px;border-bottom:1px solid #000;border-radius:0px; background-color:#FFFFFF;"
         ;>
        <div class="container-fluid" style="padding:0px;">
            <div id="navbar" class="navbar-collapse collapse" aria-expanded="true" style="height: 1px;padding:0px;">
                <ul class="nav navbar-nav">
                    <li onclick="document.location.href='http://www.pc4u.hexodo.nl/'" class="lihover"><a>Home</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">Computers
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="?p=dp">PC</a></li>
                            <li><a href="?p=lp">Laptop</a></li>
                        </ul>
                    </li>
                    <li><a href="?p=r">Reparatie</a></li>
                    <li><a href="?p=c">Contact</a></li>
                    <li><a href="?p=w">Winkelwagen</a></li>

                </ul>
            </div>
        </div>
    </nav>
    <!-- Vulling van de pagina -->
    <div id="vulling">
        <?php
        if (isset($_GET['p'])) {
            $pagina = $_GET['p'];
            switch ($pagina) {
                case "c":
                    include("contact.php");
                    break;
                case "r":
                    include("reparatie.php");
                    break;
                case "re":
                    include("registreren.php");
                    break;
                case "i":
                    include("inloggen.php");
                    break;
                case "w":
                    include("shoppingcart.php");
                    break;
                case "ch":
                    include("computerhome.php");
                    break;
                case "lp":
                    include("laptop.php");
                    break;
                case "lo":
                    include("logoutfrontend.php");
                    break;
                case "ac":
                    include("accountDetails.php");
                    break;
                case "dp":
                    include("desktop.php");
                    break;
                case "easteregg":
                    include("easteregg.php");
                    break;
                default:
                    include("home.php");
                    break;
            }
        } else {
            include("home.php");
        }
        ?>
    </div>
</div>
</body>
</html>